import {
    AfterViewInit, ApplicationRef, ChangeDetectorRef, Component, NgZone, OnChanges, OnInit,
    SimpleChanges
} from "@angular/core";
import {Router} from "@angular/router";
import {LoginService} from "./login/login.service";

declare var $:any;

@Component({
    selector: 'my-app',
    moduleId: module.id,
    templateUrl: './app.component.html',
    providers: [LoginService]
})

export class AppComponent implements OnInit {
    username: string;
    password: string;
    loginStatus: boolean = false;

    constructor(private loginService: LoginService) { }

    ngOnInit() {
        $.getScript('../assets/js/material-dashboard.js');
        $.getScript('../assets/js/initMenu.js');

        let state = localStorage.getItem('login');
        if (state && state == 'yes')
            this.loginStatus = true;
        else
            this.loginStatus = false;
    }

    login(): void {
        this.loginService.login(this.username, this.password);
    }
}
