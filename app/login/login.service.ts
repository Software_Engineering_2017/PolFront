/**
 * Created by bijan on 7/4/2017 AD.
 */

import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions, URLSearchParams} from "@angular/http";

import 'rxjs/add/operator/toPromise';

@Injectable()
export class LoginService {
    status:boolean = false;
    private url = 'http://172.21.47.105:9009/oauth/token';

    constructor(private http: Http) { }

    login(username: string, password: string): void {
        // TODO: Comment to use real login
        localStorage.setItem('login', 'yes');
        location.reload();
        return;

        let headers = new Headers();
        headers.append('Content-Type', 'application/X-www-form-urlencoded');

        let options = new RequestOptions({ headers: headers });

        let params = new URLSearchParams();
        params.set('grant_type', 'password');
        params.set('client_id', '1');
        params.set('client_secret', '9yjcmKB4rebX50OiS1lPBFJ9Frd818BR1Yq3vfdh');
        params.set('username', username);
        params.set('password', password);
        params.set('scope', '');
        console.log(params.toString());
        let request = this.http.post(this.url, params.toString(), options)
            .toPromise()
            .then(response => {
                console.log(response);
                console.log(response.status);
                if (response.status == 200) {
                    localStorage.setItem('token', response.json()['access_token']);
                    localStorage.setItem('login', 'yes');
                    location.reload();
                } else {
                    showNotification('top', 'center', 'danger', 'Sorry! Username or password is incorrect...');
                }
            })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        showNotification('top', 'center', 'danger', 'Sorry! Username or password is incorrect...');
        return Promise.reject(error.message || error);
    }
}
