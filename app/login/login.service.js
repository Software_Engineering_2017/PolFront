/**
 * Created by bijan on 7/4/2017 AD.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require('rxjs/add/operator/toPromise');
var LoginService = (function () {
    function LoginService(http) {
        this.http = http;
        this.status = false;
        this.url = 'http://172.21.47.105:9009/oauth/token';
    }
    LoginService.prototype.login = function (username, password) {
        // TODO: Comment to use real login
        localStorage.setItem('login', 'yes');
        location.reload();
        return;
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/X-www-form-urlencoded');
        var options = new http_1.RequestOptions({ headers: headers });
        var params = new http_1.URLSearchParams();
        params.set('grant_type', 'password');
        params.set('client_id', '1');
        params.set('client_secret', '9yjcmKB4rebX50OiS1lPBFJ9Frd818BR1Yq3vfdh');
        params.set('username', username);
        params.set('password', password);
        params.set('scope', '');
        console.log(params.toString());
        var request = this.http.post(this.url, params.toString(), options)
            .toPromise()
            .then(function (response) {
            console.log(response);
            console.log(response.status);
            if (response.status == 200) {
                localStorage.setItem('token', response.json()['access_token']);
                localStorage.setItem('login', 'yes');
                location.reload();
            }
            else {
                showNotification('top', 'center', 'danger', 'Sorry! Username or password is incorrect...');
            }
        })
            .catch(this.handleError);
    };
    LoginService.prototype.handleError = function (error) {
        showNotification('top', 'center', 'danger', 'Sorry! Username or password is incorrect...');
        return Promise.reject(error.message || error);
    };
    LoginService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], LoginService);
    return LoginService;
}());
exports.LoginService = LoginService;
//# sourceMappingURL=login.service.js.map