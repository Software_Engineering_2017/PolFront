"use strict";
exports.ROUTES = [
    { path: 'dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
    { path: 'user', title: 'Profile', icon: 'person', class: '' },
    { path: 'surveys', title: 'Surveys', icon: 'mode_edit', class: '' },
    { path: 'polls', title: 'Polls', icon: 'poll', class: '' },
    // { path: 'api', title: 'API',  icon:'code', class: '' },
    { path: 'reports', title: 'Reports', icon: 'content_paste', class: '' },
    // { path: 'billings', title: 'Billings',  icon:'attach_money', class: '' },
    { path: 'upgrade', title: 'Upgrade to PRO', icon: 'unarchive', class: 'active-pro' },
];
//# sourceMappingURL=sidebar-routes.config.js.map