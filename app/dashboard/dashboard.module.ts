import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {MODULE_COMPONENTS, MODULE_ROUTES} from "./dashboard.routes";
import {CommonModule} from "@angular/common";
import {HttpModule} from "@angular/http";
import {ChartsModule} from "ng2-charts";
import {FormsModule} from "@angular/forms";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        RouterModule.forChild(MODULE_ROUTES),
        ChartsModule
    ],
    declarations: [ MODULE_COMPONENTS ]
})

export class DashboardModule{}
