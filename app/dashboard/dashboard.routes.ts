import { Route } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { SurveysComponent } from './surveys/surveys.component';
import { PollsComponent } from './polls/polls.component';
import { APIComponent } from './api/api.component';
import { ReportsComponent } from './reports/reports.component';
import { BillingsComponent } from './billings/billings.component';
import { UpgradeComponent } from './upgrade/upgrade.component';
import {LineChartComponent} from "./reports/charts/line-chart.component";
import {BarChartComponent} from "./reports/charts/bar-chart.component";
import {PieChartComponent} from "./reports/charts/pie-chart.component";
import {CreatePollComponent} from "./polls/create-poll.component";

export const MODULE_ROUTES: Route[] =[
    { path: 'dashboard', component: HomeComponent },
    { path: 'user', component: UserComponent },
    { path: 'surveys', component:  SurveysComponent},
    { path: 'polls', component: PollsComponent },
    { path: 'polls/create', component: CreatePollComponent },
    { path: 'api', component: APIComponent },
    { path: 'reports', component: ReportsComponent },
    { path: 'billings', component: BillingsComponent },
    { path: 'upgrade', component: UpgradeComponent },
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' }
]

export const MODULE_COMPONENTS = [
    HomeComponent,
    UserComponent,
    SurveysComponent,
    PollsComponent,
    CreatePollComponent,
    APIComponent,
    ReportsComponent,
    BillingsComponent,
    UpgradeComponent,
    LineChartComponent,
    BarChartComponent,
    PieChartComponent
]
