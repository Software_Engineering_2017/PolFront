"use strict";
var home_component_1 = require('./home/home.component');
var user_component_1 = require('./user/user.component');
var surveys_component_1 = require('./surveys/surveys.component');
var polls_component_1 = require('./polls/polls.component');
var api_component_1 = require('./api/api.component');
var reports_component_1 = require('./reports/reports.component');
var billings_component_1 = require('./billings/billings.component');
var upgrade_component_1 = require('./upgrade/upgrade.component');
var line_chart_component_1 = require("./reports/charts/line-chart.component");
var bar_chart_component_1 = require("./reports/charts/bar-chart.component");
var pie_chart_component_1 = require("./reports/charts/pie-chart.component");
var create_poll_component_1 = require("./polls/create-poll.component");
exports.MODULE_ROUTES = [
    { path: 'dashboard', component: home_component_1.HomeComponent },
    { path: 'user', component: user_component_1.UserComponent },
    { path: 'surveys', component: surveys_component_1.SurveysComponent },
    { path: 'polls', component: polls_component_1.PollsComponent },
    { path: 'polls/create', component: create_poll_component_1.CreatePollComponent },
    { path: 'api', component: api_component_1.APIComponent },
    { path: 'reports', component: reports_component_1.ReportsComponent },
    { path: 'billings', component: billings_component_1.BillingsComponent },
    { path: 'upgrade', component: upgrade_component_1.UpgradeComponent },
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' }
];
exports.MODULE_COMPONENTS = [
    home_component_1.HomeComponent,
    user_component_1.UserComponent,
    surveys_component_1.SurveysComponent,
    polls_component_1.PollsComponent,
    create_poll_component_1.CreatePollComponent,
    api_component_1.APIComponent,
    reports_component_1.ReportsComponent,
    billings_component_1.BillingsComponent,
    upgrade_component_1.UpgradeComponent,
    line_chart_component_1.LineChartComponent,
    bar_chart_component_1.BarChartComponent,
    pie_chart_component_1.PieChartComponent
];
//# sourceMappingURL=dashboard.routes.js.map