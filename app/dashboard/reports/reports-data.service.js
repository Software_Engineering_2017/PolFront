/**
 * Created by bijan on 7/2/2017 AD.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var ReportsDataService = (function () {
    function ReportsDataService(http) {
        this.http = http;
        this.url = 'http://172.24.38.125:8000/api/v1/poll/';
    }
    ReportsDataService.prototype.getData = function (poll) {
        var chartLabels;
        var chartData;
        if (poll && poll.title == "First Poll") {
            chartLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
            chartData = [
                [65, 15, 80, 81, 56, 10, 40],
                [28, 48, 40, 56, 86, 27, 90]
            ];
        }
        else {
            chartLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
            chartData = [
                [10, 59, 80, 81, 56, 55, 40],
                [99, 48, 40, 19, 86, 27, 90]
            ];
        }
        return Promise.resolve([chartLabels, chartData]);
    };
    ReportsDataService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ReportsDataService);
    return ReportsDataService;
}());
exports.ReportsDataService = ReportsDataService;
//# sourceMappingURL=reports-data.service.js.map