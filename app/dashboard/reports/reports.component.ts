import {Component, OnInit} from '@angular/core';

import {Poll} from "../polls/poll";
import {PollService} from "../polls/poll.service";
import {ReportsDataService} from "./reports-data.service";

@Component({
    selector: 'reports-cmp',
    moduleId: module.id,
    templateUrl: 'reports.component.html',
    styleUrls: [ './reports.component.css' ],
    providers: [PollService, ReportsDataService]
})

export class ReportsComponent implements OnInit {
    surveys: any[];
    polls: Poll[];

    data: Array<string>;

    selected: string;


    constructor(private pollService: PollService, private reportsDataService: ReportsDataService) { }

    ngOnInit() {
        this.surveys = [];
        this.polls = [];
        this.getPolls();
    }

    getPolls(): void {
        this.pollService.getPolls().then(polls => this.polls = polls);
    }

    select(kind, index, item): void {
        let name = kind + '-' + index;
        if (this.selected && this.selected != name)
            document.getElementById(this.selected).classList.remove('active');
        if (!this.selected || this.selected != name)
            this.reportsDataService.getData(item).then(data => {this.data = data});
        this.selected = name;
        document.getElementById(this.selected).classList.add('active');
    }
}
