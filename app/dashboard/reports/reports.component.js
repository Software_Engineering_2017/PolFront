"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var poll_service_1 = require("../polls/poll.service");
var reports_data_service_1 = require("./reports-data.service");
var ReportsComponent = (function () {
    function ReportsComponent(pollService, reportsDataService) {
        this.pollService = pollService;
        this.reportsDataService = reportsDataService;
    }
    ReportsComponent.prototype.ngOnInit = function () {
        this.surveys = [];
        this.polls = [];
        this.getPolls();
    };
    ReportsComponent.prototype.getPolls = function () {
        var _this = this;
        this.pollService.getPolls().then(function (polls) { return _this.polls = polls; });
    };
    ReportsComponent.prototype.select = function (kind, index, item) {
        var _this = this;
        var name = kind + '-' + index;
        if (this.selected && this.selected != name)
            document.getElementById(this.selected).classList.remove('active');
        if (!this.selected || this.selected != name)
            this.reportsDataService.getData(item).then(function (data) { _this.data = data; });
        this.selected = name;
        document.getElementById(this.selected).classList.add('active');
    };
    ReportsComponent = __decorate([
        core_1.Component({
            selector: 'reports-cmp',
            moduleId: module.id,
            templateUrl: 'reports.component.html',
            styleUrls: ['./reports.component.css'],
            providers: [poll_service_1.PollService, reports_data_service_1.ReportsDataService]
        }), 
        __metadata('design:paramtypes', [poll_service_1.PollService, reports_data_service_1.ReportsDataService])
    ], ReportsComponent);
    return ReportsComponent;
}());
exports.ReportsComponent = ReportsComponent;
//# sourceMappingURL=reports.component.js.map