/**
 * Created by bijan on 7/2/2017 AD.
 */

import {Injectable} from "@angular/core";
import {Http} from "@angular/http";

import "rxjs/add/operator/toPromise";
import {Poll} from "../polls/poll";

@Injectable()
export class ReportsDataService {
    private url = 'http://172.24.38.125:8000/api/v1/poll/';

    constructor(private http: Http) { }

    getData(poll: Poll): Promise<Array<any>> {
        let chartLabels:Array<any>;
        let chartData: Array<any>;

        if (poll && poll.title == "First Poll") {
            chartLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
            chartData = [
                [65, 15, 80, 81, 56, 10, 40],
                [28, 48, 40, 56, 86, 27, 90]
            ];
        } else {
            chartLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
            chartData = [
                [10, 59, 80, 81, 56, 55, 40],
                [99, 48, 40, 19, 86, 27, 90]
            ];
        }

        return Promise.resolve([chartLabels, chartData]);
    }

    // private handleError(error: any): Promise<any> {
    //     console.error('An error occurred', error); // for demo purposes only
    //     return Promise.reject(error.message || error);
    // }
}
