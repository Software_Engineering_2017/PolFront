/**
 * Created by bijan on 7/3/2017 AD.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var ng2_charts_1 = require("ng2-charts");
var BarChartComponent = (function () {
    function BarChartComponent() {
    }
    BarChartComponent.prototype.ngOnInit = function () { };
    BarChartComponent.prototype.ngOnChanges = function (changes) {
        var _this = this;
        if (changes.hasOwnProperty('data') && this.data) {
            this.chartLabels = this.data[0];
            this.chartData = this.data[1];
            setTimeout(function () { _this._chart.refresh(); }, 10);
        }
    };
    BarChartComponent.prototype.chartClicked = function (e) {
        console.log(e);
    };
    BarChartComponent.prototype.chartHovered = function (e) {
        console.log(e);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], BarChartComponent.prototype, "data", void 0);
    __decorate([
        core_1.ViewChild(ng2_charts_1.BaseChartDirective), 
        __metadata('design:type', Object)
    ], BarChartComponent.prototype, "_chart", void 0);
    BarChartComponent = __decorate([
        core_1.Component({
            selector: 'bar-chart',
            moduleId: module.id,
            templateUrl: './bar-chart.component.html',
        }), 
        __metadata('design:paramtypes', [])
    ], BarChartComponent);
    return BarChartComponent;
}());
exports.BarChartComponent = BarChartComponent;
//# sourceMappingURL=bar-chart.component.js.map