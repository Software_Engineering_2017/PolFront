/**
 * Created by bijan on 6/29/2017 AD.
 */

import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from "@angular/core";
import {BaseChartDirective} from "ng2-charts";

@Component({
    selector: 'line-chart',
    moduleId: module.id,
    templateUrl: './line-chart.component.html',
})
export class LineChartComponent implements OnInit, OnChanges {
    @Input() data: Array<any>;
    public chartLabels: Array<any>;
    public chartData: Array<any>;

    @ViewChild(BaseChartDirective) private _chart;

    ngOnInit(): void { }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.hasOwnProperty('data') && this.data) {
            this.chartLabels = this.data[0];
            this.chartData = this.data[1];
            setTimeout(
                () => {this._chart.refresh();},
                10
            );
        }
    }

    public chartClicked(e:any):void {
        console.log(e);
    }

    public chartHovered(e:any):void {
        console.log(e);
    }
}
