/**
 * Created by bijan on 7/3/2017 AD.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var poll_service_1 = require('./poll.service');
var router_1 = require("@angular/router");
var CreatePollComponent = (function () {
    function CreatePollComponent(router, pollService) {
        this.router = router;
        this.pollService = pollService;
    }
    CreatePollComponent.prototype.ngOnInit = function () {
        this.choices = [null, null];
    };
    CreatePollComponent.prototype.addChoice = function () {
        this.choices.push(null);
    };
    CreatePollComponent.prototype.removeChoice = function (i) {
        if (this.choices.length > 2)
            this.choices.splice(i, 1);
        else {
            showNotification('top', 'center', 'warning', 'Sorry but every poll should have at least 2 choices.');
        }
    };
    CreatePollComponent.prototype.trackByFn = function (index, item) {
        return index;
    };
    CreatePollComponent.prototype.goBack = function () {
        this.router.navigateByUrl('polls');
    };
    CreatePollComponent = __decorate([
        core_1.Component({
            selector: 'create-poll-cmp',
            moduleId: module.id,
            templateUrl: 'create-poll.component.html',
            styleUrls: ['./create-poll.component.css'],
            providers: [poll_service_1.PollService]
        }), 
        __metadata('design:paramtypes', [router_1.Router, poll_service_1.PollService])
    ], CreatePollComponent);
    return CreatePollComponent;
}());
exports.CreatePollComponent = CreatePollComponent;
//# sourceMappingURL=create-poll.component.js.map