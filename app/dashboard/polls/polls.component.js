"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var poll_service_1 = require('./poll.service');
var router_1 = require("@angular/router");
var PollsComponent = (function () {
    function PollsComponent(router, pollService) {
        this.router = router;
        this.pollService = pollService;
    }
    PollsComponent.prototype.ngOnInit = function () {
        this.getPolls();
    };
    PollsComponent.prototype.getPolls = function () {
        var _this = this;
        this.pollService.getPolls().then(function (polls) { return _this.polls = polls; });
    };
    PollsComponent.prototype.navigateToCreate = function () {
        this.router.navigateByUrl('polls/create');
    };
    PollsComponent = __decorate([
        core_1.Component({
            selector: 'polls-cmp',
            moduleId: module.id,
            templateUrl: 'polls.component.html',
            styleUrls: ['./polls.component.css'],
            providers: [poll_service_1.PollService]
        }), 
        __metadata('design:paramtypes', [router_1.Router, poll_service_1.PollService])
    ], PollsComponent);
    return PollsComponent;
}());
exports.PollsComponent = PollsComponent;
//# sourceMappingURL=polls.component.js.map