/**
 * Created by bijan on 6/27/2017 AD.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require('rxjs/add/operator/toPromise');
var mock_polls_1 = require("./mock-polls");
var PollService = (function () {
    function PollService(http) {
        this.http = http;
        this.url = 'http://172.21.47.105:9009/api/v1/polls';
    }
    PollService.prototype.getPolls = function () {
        // TODO: Comment to get data from server
        return Promise.resolve(mock_polls_1.POLLS);
        var token = 'Bearer ' + localStorage.getItem('token');
        console.log(token);
        var headers = new http_1.Headers();
        headers.append('Authorization', token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.get(this.url, options)
            .toPromise()
            .then(function (response) { return response.json().data; })
            .catch(this.handleError);
    };
    PollService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    PollService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], PollService);
    return PollService;
}());
exports.PollService = PollService;
//# sourceMappingURL=poll.service.js.map