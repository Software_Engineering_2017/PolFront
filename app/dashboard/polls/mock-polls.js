/**
 * Created by bijan on 6/27/2017 AD.
 */
"use strict";
exports.POLLS = [
    { id: 1, title: 'First Poll', description: 'First Description', is_private: true },
    { id: 2, title: 'Second Poll', description: 'Second Description', is_private: false },
    { id: 3, title: 'Third Poll', description: 'Third Description', is_private: true },
    { id: 4, title: 'Fourth Poll', description: 'Fourth Description', is_private: false },
    { id: 5, title: 'Fifth Poll', description: 'Fifth Description', is_private: true }
];
//# sourceMappingURL=mock-polls.js.map