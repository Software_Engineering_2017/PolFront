/**
 * Created by bijan on 6/27/2017 AD.
 */

export class Poll {
    id: number;
    title: string;
    // url: string;
    // expire_date: string;
    // language: string;
    description: string;
    is_private: boolean;
}
