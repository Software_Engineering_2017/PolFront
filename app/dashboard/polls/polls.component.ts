import { Component, OnInit } from '@angular/core';
import { Poll } from './poll';
import { PollService } from './poll.service'
import {Router} from "@angular/router";

@Component({
    selector: 'polls-cmp',
    moduleId: module.id,
    templateUrl: 'polls.component.html',
    styleUrls: [ './polls.component.css' ],
    providers: [PollService]
})

export class PollsComponent implements OnInit {
    polls: Poll[];

    constructor(private router: Router, private pollService: PollService) { }

    ngOnInit() {
        this.getPolls();
    }

    getPolls(): void {
        this.pollService.getPolls().then(polls => this.polls = polls);
    }

    navigateToCreate(): void {
        this.router.navigateByUrl('polls/create');
    }
}
