/**
 * Created by bijan on 6/27/2017 AD.
 */

import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions} from "@angular/http";

import 'rxjs/add/operator/toPromise';

import {Poll} from "./poll";
import {POLLS} from "./mock-polls";

@Injectable()
export class PollService {
    private url = 'http://172.21.47.105:9009/api/v1/polls';

    constructor(private http: Http) { }

    getPolls(): Promise<Poll[]> {
        // TODO: Comment to get data from server
        return Promise.resolve(POLLS);

        let token = 'Bearer ' + localStorage.getItem('token');
        console.log(token);
        let headers = new Headers();
        headers.append('Authorization', token);

        let options = new RequestOptions({ headers: headers });

        return this.http.get(this.url, options)
            .toPromise()
            .then(response => response.json().data as Poll[])
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}
