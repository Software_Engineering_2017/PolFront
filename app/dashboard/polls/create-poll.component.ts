/**
 * Created by bijan on 7/3/2017 AD.
 */

import {Component, OnInit} from '@angular/core';
import { Poll } from './poll';
import { PollService } from './poll.service'
import {Router} from "@angular/router";

@Component({
    selector: 'create-poll-cmp',
    moduleId: module.id,
    templateUrl: 'create-poll.component.html',
    styleUrls: [ './create-poll.component.css' ],
    providers: [PollService]
})

export class CreatePollComponent implements OnInit {
    choices: Array<string>;
    constructor(private router:Router, private pollService: PollService) { }

    ngOnInit() {
        this.choices = [null, null];
    }

    addChoice() {
        this.choices.push(null);
    }

    removeChoice(i: number) {
        if (this.choices.length > 2)
            this.choices.splice(i, 1);
        else {
            showNotification('top', 'center', 'warning', 'Sorry but every poll should have at least 2 choices.');
        }
    }

    trackByFn(index: any, item: any) {
        return index;
    }

    goBack(): void {
        this.router.navigateByUrl('polls');
    }
}
