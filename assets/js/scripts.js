/**
 * Created by bijan on 7/3/2017 AD.
 */

function showNotification(from, align, type, txt) {

    $.notify({
        icon: "info",
        message: txt

    },{
        type: type,
        timer: 1000,
        placement: {
            from: from,
            align: align
        }
    });
}
